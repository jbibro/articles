from django.contrib import admin
from articles.models import Article, Category


def is_publisher(request):
    return request.user.groups.filter(name='publisher').exists()


def is_editor(request):
    return request.user.groups.filter(name='editor').exists()


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    fields = ('title', 'category', 'editor', 'published', 'content')
    readonly_fields = ['editor', 'published']
    list_display = (
        'title',
        'published'
    )
    actions = ['publish', 'unpublish']

    def save_model(self, request, obj, form, change):
        if not obj.editor and is_editor(request):
            obj.editor = request.user
        super().save_model(request, obj, form, change)

    def get_readonly_fields(self, request, obj=None):
        all_fields = ['title', 'content', 'category']
        editable_fields = []
        if obj is None:
            return self.readonly_fields

        if obj.edited_by(request.user):
            editable_fields = all_fields

        if is_publisher(request):
            editable_fields += ['published']

        return list(set(all_fields) - set(editable_fields)) + self.readonly_fields

    def has_delete_permission(self, request, obj=None):
        if obj is None:
            return True

        if obj.edited_by(request.user):
            return True

        return False

    def get_actions(self, request):
        actions = super().get_actions(request)
        if not is_publisher(request):
            del actions['publish']
            del actions['unpublish']
        return actions

    def publish(self, request, queryset):
        queryset.update(published=True)

    def unpublish(self, request, queryset):
        queryset.update(published=False)


admin.site.register(Category)
