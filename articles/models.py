from auditlog.registry import auditlog
from django.db import models
from django.contrib.auth.models import User
from django.db.models import SET_NULL


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'categories'


class Article(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField(null=True, blank=True)
    category = models.ManyToManyField(Category)
    editor = models.ForeignKey(User, on_delete=SET_NULL, null=True)
    published = models.BooleanField(default=False)

    def edited_by(self, user):
        return self.editor == user and not self.published

    def __str__(self):
        return self.title


auditlog.register(Article)
