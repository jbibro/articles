FROM python:3.6
RUN mkdir /code
WORKDIR /code
ADD . /code/
EXPOSE 8000
RUN pip install -r requirements.txt
RUN python manage.py migrate
RUN python manage.py loaddata fixtures.json
CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000"]